# -*- coding: utf-8 -*-
"""Polish Market research.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1dUDDy5r-u2klNBZHw3Eq-bNIpp-Vnme2

# Pobieranie i importy
"""

!pip install snscrape

#manipulacja danymi
import pandas as pd
import numpy as np
import math

#wizualizacja
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns

#scraping
import snscrape.modules.twitter as sntwitter
import requests
import re

#czasowe
from time import sleep
from datetime import date
import datetime

#przygotowanie danych
from sklearn.preprocessing import MinMaxScaler

"""# Definiowanie Pozyskiwania danych

##Wiadomości z Tweetera i portali

###Twitter
"""

def get_tweets_by_query(query, no_tweets):
  tweets = []
  for tweet in sntwitter.TwitterSearchScraper(query).get_items():
    if len(tweets) == no_tweets:
      break
    else:
      tweets.append([tweet.date, tweet.username, tweet.content])

  print(f'Pobrano Tweety zawierające: {query}')
  return tweets


def get_tweets_by_queries(no_tweets):
  queries = ['giełda', 'GPW', 'WIG', 'WIG20', 'WIG30', 'sWIG80', 'mWIG40', 'NewConect', 'spółka', 'cena', 'kredyt', 'firma', 
              'gospodarka', 'rynek', 'akcje', 'waluta', 'surowce', 'nieruchomości', 'fabryka', 'bezrobocie', 
             'wskaźnik', 'przemysł', 'zamówienia', 'przedsiębiorstwa', 'produkcja' ] #lista zapytań do pobrania
  tweets = []
  for query in queries:
    tweets.extend(get_tweets_by_query(query, no_tweets))

  df = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet'])
  return df


#-------------------------------------------------------------



def get_tweets_by_user(user_name, no_tweets):
  tweets = []
  user = f'(from:{user_name})'
  for tweet in sntwitter.TwitterSearchScraper(user).get_items():
    if len(tweets) == no_tweets:
      break
    else:
      tweets.append([tweet.date, tweet.username, tweet.content])

  print(f'Pobrano Tweety Użytkownika: {user_name}')
  return tweets


def get_tweets_by_users(no_tweets):
  users = ['StockWatchPL','pl_krn','MikRaczynski', 'mbank_research', 'PrzemekSNR', 'pbujak', 'strefainw', 'DPackowski',
           'TrysteroBlog', 'analizy_pl', 'Wilk_z_GPW', 'biznesradar', 'comparic', 'JakInwestowac','RPEkonomia', 'BIPolska',
           'OFinansowy', 'puls_biznesu', 'GPW_WSExchange', 'BiznesAlert', 'PKO_Research', 'GUS_STAT'] #lista użytkowników do pobrania
  tweets = []
  for user in users:
    tweets.extend(get_tweets_by_user(user, no_tweets))

  df = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet'])
  return df


#-------------------------------------------------------------



def get_twitter_data(no_tweets):
  
  users = get_tweets_by_users(no_tweets)
  #queries = get_tweets_by_queries(no_tweets)
  #tweeter_data = pd.concat([users, queries], ignore_index=True)
  #tweeter_data['Date'] = tweeter_data['Date'].apply(lambda x: pd.Timestamp(x).strftime('%Y-%m-%d'))
  users['Date'] = users['Date'].apply(lambda x: pd.Timestamp(x).strftime('%Y-%m-%d'))
  
  return users
  #return tweeter_data

"""###Biznes Radar"""

def get_biznesradar(number_of_pages):
    
    all_dates = []
    all_titles = []
    all_content = []
    
    for page_number in range(1, number_of_pages+1):
        print(f'Pobieram informacje gospodarcze z Biznes Radar - strona numer {page_number}')
        
        http_address = f'https://www.biznesradar.pl/news-archive/1,{page_number}'
        response = requests.get(http_address)
        content = response.content.decode("utf-8")

        dates = re.findall(r'"record-date">(.+?)\s\d\d.\d\d.\d\d</span>', content, re.DOTALL)
        all_dates.extend(dates)
        

        titles = re.findall(r'<a target="_blank" href.+?rel="nofollow">(.+?)</a>', content, re.DOTALL)
        all_titles.extend(titles)
        
        content = re.findall(r'<div class="record-body">\n\t\t\t\s+(.+?)\n\t\t', content, re.DOTALL)
        all_content.extend(content)
        
        sleep(1)
    
    total = {'Date':all_dates, 'Title': all_titles, 'Content': all_content}
    total =  pd.DataFrame(total)
    #total['Date'] = total['Date'].apply(lambda x: pd.Timestamp(x).strftime('%Y-%m-%d'))
    return total

"""###bankier.pl"""

def get_bankierpl_gospodarka(number_of_pages):
    
    all_dates = []
    all_titles = []
    all_content = []
    
    for page_number in range(1, number_of_pages+1):
        print(f'Pobieram informacje gospodarcze z bankier.pl - strona numer {page_number}')
        
        http_address = f'https://www.bankier.pl/gospodarka/wiadomosci/{page_number}' #max100
        response = requests.get(http_address)
        content = response.content.decode("utf-8")

        data = re.findall(r'<time class="entry-date".+?pubdate>(.+?)\s\d\d.\d\d</time>\n', content, re.DOTALL)
        y = 0
        for x in data:
          if y==10:
            break
          else:
            all_dates.append(x)
            y+=1

        titles = re.findall(r'rel="bookmark">\n\s+(.+?)\n\n', content, re.DOTALL)
        all_titles.extend(titles)
        
        content = re.findall(r'</span>\n\n\s+<p>(.+?)<a href', content, re.DOTALL)
        all_content.extend(content)
        
        sleep(1)
    
    total = {'Date':all_dates, 'Title': all_titles, 'Content': all_content}
    return pd.DataFrame(total)

#-------------------------------------------------------------------------------------

def get_bankierpl_rynki(number_of_pages):
    
    all_dates = []
    all_titles = []
    all_content = []
    
    for page_number in range(1, number_of_pages+1):
        print(f'Pobieram komentarze rynkowe z bankier.pl - strona numer {page_number}')
        
        http_address = f'https://www.bankier.pl/rynki/wiadomosci/{page_number}' #max 65
        response = requests.get(http_address)
        content = response.content.decode("utf-8")

        data = re.findall(r'<time class="entry-date".+?pubdate>(.+?)\s\d\d.\d\d</time>\n', content, re.DOTALL)
        y = 0
        for x in data:
          if y==15:
            break
          else:
            all_dates.append(x)
            y+=1

        titles = re.findall(r'rel="bookmark">\n\s+(.+?)\n\n', content, re.DOTALL)
        all_titles.extend(titles)
        
        content = re.findall(r'</span>\n\n\s+<p>(.+?)<a href', content, re.DOTALL)
        all_content.extend(content)
        
        sleep(1)
    
    total = {'Date':all_dates, 'Title': all_titles, 'Content': all_content}
    return pd.DataFrame(total)


def get_bankierpl(number_of_pages):

  gospodarka = get_bankierpl_gospodarka(number_of_pages)
  rynki = get_bankierpl_rynki(number_of_pages)
  dane = pd.concat([gospodarka, rynki], ignore_index=True)
  #dane['Date'] = dane['Date'].apply(lambda x: pd.Timestamp(x).strftime('%Y-%m-%d'))
  dane = dane.sort_values(by='Date', ignore_index=True)
  return dane

"""###get_portals"""

def get_portals(number_of_pages):
  biznesradar = get_biznesradar(number_of_pages)
  bankier = get_bankierpl(number_of_pages)
  dane = pd.concat([biznesradar, bankier], ignore_index=True)
  dane.sort_values(by="Date", ascending = False, ignore_index=True)
  return dane, biznesradar, bankier

"""##Spółki WIG20"""

tiki = ['acp', 'ale', 'ccc', 'cdr', 'cps', 'dnp', 
        'jsw', 'kgh', 'kty', 'lpp', 'mbk', 'opl', 'pco', 
        'peo', 'pge', 'pgn', 'pkn', 'pko', 'pzu', 'spl']

tiki_zamkniecie = []
for tik in tiki:
  tiki_zamkniecie.append(f'{tik} Zamkniecie')

tiki_zmiana = []
for tik in tiki:
  tiki_zmiana.append(f'{tik} Zmiana')

tiki_wolumen = []
for tik in tiki:
  tiki_wolumen.append(f'{tik} Wolumen')


# Pojedyncza spółka wszystkie dane

def get_company(tick):
  
    print(f'Pobieram ceny: {tick}')
    path = f'https://stooq.pl/q/d/l/?s={tick}&i=d'
    dataset = pd.read_csv(path)
    dataset['Data'] = pd.to_datetime(dataset['Data'])
    dataset['Zmiana'] = dataset["Zamkniecie"].pct_change()
    dataset = dataset[['Data', 'Zamkniecie', 'Wolumen', 'Zmiana']]
    dataset.columns = ['Data', f'{tick} Zamkniecie', f'{tick} Wolumen', f'{tick} Zmiana']
    dataset = dataset.set_index('Data')
    dataset = dataset[-2500:] #ostatnie 2500 dni pracujących / ok 10 lat
    return dataset

# Wszystkie spółki wszystkie dane -----------------------------------------------------------------------

def get_stocks():
  acp = get_company('acp') 
  ale = get_company('ale') #12 październik 2020
  ccc = get_company('ccc')
  cdr = get_company('cdr')
  cps = get_company('cps')
  dnp = get_company('dnp') # 19 kwietnia 2017
  jsw = get_company('jsw') 
  kgh = get_company('kgh')
  kty = get_company('kty') #jeden dzień brak notowań
  lpp = get_company('lpp')
  mbk = get_company('mbk')
  opl = get_company('opl')
  pco = get_company('pco') # 26 maj 2021
  peo = get_company('peo')
  pge = get_company('pge')
  pgn = get_company('pgn')
  pkn = get_company('pkn')
  pko = get_company('pko')
  pzu = get_company('pzu')
  spl = get_company('spl') #jeden dzień brak notowań
  wig20 = get_company('wig20')
  wig = get_company('wig')
  all = pd.concat([acp, ale, ccc, cdr, cps, dnp, 
        jsw, kgh, kty, lpp, mbk, opl, pco, 
        peo, pge, pgn, pkn, pko, pzu, spl], axis=1)

  return all, wig20, wig

#tylko zmiana procentowa -----------------------------------------------------------------------

def get_change(tick):
  
    print(f'Pobieram dzienne zmiany procentowe: {tick}')
    path = f'https://stooq.pl/q/d/l/?s={tick}&i=d'
    dataset = pd.read_csv(path)
    dataset['Data'] = pd.to_datetime(dataset['Data'])
    dataset['Zmiana'] = dataset["Zamkniecie"].pct_change()
    dataset = dataset[['Data', 'Zmiana']]
    dataset.columns = ['Data', f'{tick}']
    dataset = dataset.set_index('Data')
    dataset = dataset[-2500:] #ostatnie 2500 dni pracujących / ok 10 lat
    return dataset

def get_price_change():
  acp = get_change('acp') 
  ale = get_change('ale') #12 październik 2020
  ccc = get_change('ccc')
  cdr = get_change('cdr')
  cps = get_change('cps')
  dnp = get_change('dnp') # 19 kwietnia 2017
  jsw = get_change('jsw') 
  kgh = get_change('kgh')
  kty = get_change('kty') #jeden dzień brak notowań
  lpp = get_change('lpp')
  mbk = get_change('mbk')
  opl = get_change('opl')
  pco = get_change('pco') # 26 maj 2021
  peo = get_change('peo')
  pge = get_change('pge')
  pgn = get_change('pgn')
  pkn = get_change('pkn')
  pko = get_change('pko')
  pzu = get_change('pzu')
  spl = get_change('spl') #jeden dzień brak notowań

  all = pd.concat([acp, ale, ccc, cdr, cps, dnp, 
        jsw, kgh, kty, lpp, mbk, opl, pco, 
        peo, pge, pgn, pkn, pko, pzu, spl], axis=1)
  
  return all


#tylko wolumeny ------------------------------------------------------------------------

def get_volumen(tick):
  
    print(f'Pobieram dzienne wolumeny obrotu: {tick}')
    path = f'https://stooq.pl/q/d/l/?s={tick}&i=d'
    dataset = pd.read_csv(path)
    dataset['Data'] = pd.to_datetime(dataset['Data'])
    dataset = dataset[['Data', 'Wolumen']]
    dataset.columns = ['Data', f'{tick}']
    dataset = dataset.set_index('Data')
    dataset = dataset[-2500:] #ostatnie 2500 dni pracujących / ok 10 lat
    return dataset

def get_all_volumen():
  acp = get_volumen('acp') 
  ale = get_volumen('ale') #12 październik 2020
  ccc = get_volumen('ccc')
  cdr = get_volumen('cdr')
  cps = get_volumen('cps')
  dnp = get_volumen('dnp') # 19 kwietnia 2017
  jsw = get_volumen('jsw') 
  kgh = get_volumen('kgh')
  kty = get_volumen('kty') #jeden dzień brak notowań
  lpp = get_volumen('lpp')
  mbk = get_volumen('mbk')
  opl = get_volumen('opl')
  pco = get_volumen('pco') # 26 maj 2021
  peo = get_volumen('peo')
  pge = get_volumen('pge')
  pgn = get_volumen('pgn')
  pkn = get_volumen('pkn')
  pko = get_volumen('pko')
  pzu = get_volumen('pzu')
  spl = get_volumen('spl') #jeden dzień brak notowań

  all = pd.concat([acp, ale, ccc, cdr, cps, dnp, 
        jsw, kgh, kty, lpp, mbk, opl, pco, 
        peo, pge, pgn, pkn, pko, pzu, spl], axis=1)
  
  return all

"""# Pobieranie danych

##portale
"""

portals, biznesradar, bankier = get_portals(65) #max stron na bankier rynki

"""##Twitter"""

twitter = get_twitter_data(200)

"""##Giełda"""

stocks, wig20, wig = get_stocks()

"""#Wizualizacje ilości

##Ilość

###Wszystkie dane - ilość
"""

fig, ax = plt.subplots(figsize=(15,5))
plt.bar("Twitter", len(twitter),)
plt.bar("Biznes Radar", len(biznesradar))
plt.bar("Bankier.pl", len(bankier))
ax.set_xlabel("Źródło")
ax.set_ylabel('Ilość')
plt.title('Ilość danych w zależności od źródła')
plt.show()

"""###Same portale - podział na ilość"""

fig, ax = plt.subplots(figsize=(15,5))
plt.bar("Biznes Radar", len(biznesradar))
plt.bar("Bankier.pl", len(bankier))
ax.set_xlabel("Źródło")
ax.set_ylabel('Ilość')
plt.title('Ilość - same portale')
plt.show()

"""## występowanie w czasie

###Twitter
"""

#tabela pokazująca ilość występowania w danych dniach
tw_uni_date = twitter['Date'].unique()
tw_dates = twitter['Date']
tw_uni_count = []
for x in tw_uni_date:
  num = 0
  for y in tw_dates:
    if x == y:
      num += 1
  tw_uni_count.append(num)
tweeter_by_date = {'Date':tw_uni_date, 'Count': tw_uni_count,}
tweeter_by_date = pd.DataFrame(tweeter_by_date)
tweeter_by_date['Date'] = tweeter_by_date['Date'].apply(lambda x: pd.Timestamp(x).strftime('%Y-%m-%d'))
tweeter_by_date = tweeter_by_date.sort_values(by='Date', ignore_index=True)

"""Wszytskie tweety w czasie"""

fig, ax = plt.subplots(figsize=(20,7))
plt.bar(tweeter_by_date.Date, tweeter_by_date.Count)
ax.xaxis.set_tick_params(rotation=90)
ax.set_xlabel("Czas")
ax.set_ylabel('Ilość')
plt.title("Wszystkie Tweety w czasie")
plt.show()

"""Tweety - ostatnie 30 dni"""

fig, ax = plt.subplots(figsize=(20,7))
plt.bar(tweeter_by_date.Date[-30:], tweeter_by_date.Count[-30:])
ax.xaxis.set_tick_params(rotation=70)
ax.set_xlabel("Czas")
ax.set_ylabel('Ilość')
plt.title("Tweety w czasie ostatnich 30 dni ")
plt.show()

"""###Portale"""

#tabela pokazująca ilość występowania w danych dniach
def portal_count_by_date(portal):
  uni_date = portal['Date'].unique()
  dates = portal['Date']
  uni_count = []
  for x in uni_date:
    num = 0
    for y in dates:
      if x == y:
        num += 1
    uni_count.append(num)
  by_date = {'Date':uni_date, 'Count': uni_count}
  by_date = pd.DataFrame(by_date)
  by_date = by_date.sort_values(by='Date', ignore_index=True)
  return by_date

biznes_by_date = portal_count_by_date(biznesradar)
bankier_by_date = portal_count_by_date(bankier)

"""Biznes Radar"""

fig, ax = plt.subplots(figsize=(20,7))
ax.bar(biznes_by_date.Date, biznes_by_date.Count)
ax.xaxis.set_tick_params(rotation=70)
ax.set_xlabel("Czas")
ax.set_ylabel('Ilość')
plt.title("Biznes radar - ilość")
plt.show()

"""bankier.pl"""

fig, ax = plt.subplots(figsize=(20,7))
ax.bar(bankier_by_date.Date, bankier_by_date.Count)
ax.xaxis.set_tick_params(rotation=70)
ax.set_xlabel("Czas")
ax.set_ylabel('Ilość')
plt.title("bankier.pl - ilość")
plt.show()

"""#Ogarnianie textu

##Usuwanie user z tweetera
"""

twitter_no_user = twitter[['Date','Tweet']]
twitter_no_user = twitter_no_user.sort_values(by='Date', ignore_index=True, ascending=False)
twitter_no_user

"""##Łączenie nagłówka i początku artykułów z portali"""

portals["combine"] = portals['Title'].astype(str) +" "+ portals["Content"]
portals_combine = portals[['Date', 'combine']]
portals_combine

"""##Łączenie 2 baz ze sobą"""

twitter_no_user.rename(columns = {'Tweet':'Text'}, inplace = True)
portals_combine.rename(columns = {'combine':'Text'}, inplace = True)
frames = [twitter_no_user, portals_combine]
all = pd.concat(frames, ignore_index=True )

all = all.sort_values(by='Date', ignore_index=True, ascending=False)

all.sample(20)

"""##PL Stop words - usuwanie"""

!pip install -U pip setuptools wheel
!pip install -U spacy
!python3 -m spacy download pl_core_news_sm

import spacy
import pl_core_news_sm

import nltk
nltk.download('stopwords')
nltk.download('averaged_perceptron_tagger')
nltk.download('punkt')
from nltk.tokenize import sent_tokenize, word_tokenize

pl_stop_words = ['a', 'aby', 'ach', 'acz', 'aczkolwiek', 'aj', 'albo', 'ale', 'ależ', 'ani', 'aż', 
                 'bardziej', 'bardzo', 'bo', 'bowiem', 'by', 'byli', 'bynajmniej', 'być', 'był', 'była', 
                 'było', 'były', 'będzie', 'będą', 'cali', 'cała', 'cały', 'ci', 'cię', 'ciebie', 'co','cokolwiek', 
                 'coś', 'czasami', 'czasem', 'czemu', 'czy', 'czyli', 'daleko', 'dla', 'dlaczego', 
                 'dlatego', 'do', 'dobrze', 'dokąd', 'dość', 'dużo', 'dwa', 'dwaj', 'dwie', 'dwoje', 'dziś', 'dzisiaj', 
                 'gdy', 'gdyby', 'gdyż', 'gdzie', 'gdziekolwiek', 'gdzieś', 'i', 'ich', 'ile', 'im', 'inna', 'inne', 'inny', 
                 'innych', 'iż', 'ja', 'ją', 'jak', 'jakaś', 'jakby', 'jaki', 'jakichś', 'jakie', 'jakiś', 'jakiż', 
                 'jakkolwiek', 'jako', 'jakoś', 'je', 'jeden', 'jedna', 'jedno', 'jednak', 'jednakże', 'jego', 'jej', 'jemu', 
                 'jest', 'jestem', 'jeszcze', 'jeśli', 'jeżeli', 'już', 'ją', 'każdy', 'kiedy', 'kilka', 'kimś', 'kto', 
                 'ktokolwiek', 'ktoś', 'która', 'które', 'którego', 'której', 'który', 'których', 'którym', 'którzy', 'ku', 
                 'lat', 'lecz', 'lub', 'ma', 'mają', 'mieć', 'móc', 'mało', 'mam', 'mi', 'mimo', 'między', 'mną', 'mnie', 'mogą', 'moi', 
                 'moim', 'moja', 'moje', 'może', 'możliwe', 'można', 'mój', 'mu', 'musi', 'my', 'na', 'nad', 'nam', 'nami', 
                 'nas', 'nasi', 'nasz', 'nasza', 'nasze', 'naszego', 'naszych', 'natomiast', 'natychmiast', 'nawet', 'nią', 
                 'nic', 'nich', 'nie', 'niech', 'niego', 'niej', 'niemu', 'nigdy', 'nim', 'nimi', 'niż', 'no', 'o', 'obok', 
                 'od', 'około', 'on', 'ona', 'one', 'oni', 'ono', 'oraz', 'oto', 'owszem', 'pan', 'pana', 'pani', 'po', 
                 'pod', 'podczas', 'pomimo', 'ponad', 'ponieważ', 'powinien', 'powinna', 'powinni', 'powinno', 'poza', 'prawie', 
                 'przecież', 'przed', 'przede', 'przedtem', 'przez', 'przy', 'roku', 'również', 'sama', 'są', 'się', 'skąd', 
                 'sobie', 'sobą', 'sposób', 'swoje', 'ta', 'tak', 'taka', 'taki', 'takie', 'także', 'tam', 'te', 'tego', 'tej', 
                 'temu', 'ten', 'teraz', 'też', 'to', 'tobą', 'tobie', 'toteż', 'trzeba', 'tu', 'tutaj', 'twoi', 'twoim', 'twoja', 
                 'twoje', 'twym', 'twój', 'ty', 'tych', 'tylko', 'tym', 'u', 'w', 'wam', 'wami', 'was', 'wasz', 'wasza', 'wasze', 
                 'we', 'według', 'wiele', 'wielu', 'więc', 'więcej', 'wszyscy', 'wszystkich', 'wszystkie', 'wszystkim', 'wszystko', 
                 'wtedy', 'wy', 'właśnie', 'z', 'za', 'zapewne', 'zawsze', 'ze', 'zł', 'znowu', 'znów', 'został', 'żaden', 'żadna', 
                 'żadne', 'żadnych', 'że', 'żeby', '@', '#', '$', '!', '%', '.', ',', '?', '-', ':', '"', ';', '&', ')', '(', 'https',
                 '//t.co', '/', '-', '...', '—', '_' 't', 'm', 'in', 'SWIG80', 'WIG20', 'raz', 'jeden', 'miesiąc', 'styczeń', 'luty', 'marzec', 
                 'kwiecień', 'maj', 'czerwiec', 'lipiec', 'sierpień', 'wrzesień', 'październik', 'listopad', 'grudzień', 'quot', 'WIG', 
                 'wig', 'rok', 'GPW', 'dzień', 'tydzień', '\n', '…', '`','the', 'is', 'of', 'for', 'it', 'ii', 'q', 'and', 'm', 'q' 'kw', 's'
                 's', 'poniedziałek', 'wtorek', 'środa', 'czwartek', 'piątek', 'sobota', 'niedziela', 'Polska', 'polski']

all_no_stop = []
all_lower =[]
a = []
for text in all['Text']:
  a.append(word_tokenize(text))


for text in a:
  b = []
  for word in text:
    x = word.lower()
    b.append(x)
  all_lower.append(b)
  
for text in all_lower:
  c = []
  for word in text:
     if word not in pl_stop_words:
      c.append(word)
  all_no_stop.append(' '.join(c))

all_no_stop[300]

len(all_no_stop)

"""##Usuwanie linków"""

all_no_stop_no_link = []
regex = r"//t.co/.........."
for sentence in all_no_stop:
  a = re.sub(regex, '', sentence)
  all_no_stop_no_link.append(a)

all_no_stop_no_link[300]

len(all_no_stop_no_link)

all['Text'] = all_no_stop_no_link

all.sample(20)

"""##Lemmantyzacja

###Stokenizowane, zlemantyzowane i wolne od stop wordsów pojedyncze słowa
"""

nlp = spacy.load('pl_core_news_sm')

words_lemma = []
for x in all['Text']:
  doc = nlp(x)
  for word in doc:
    words_lemma.append(word.lemma_)

words_lemma_no_stop =[]
for word in words_lemma:
  if word not in pl_stop_words:
    words_lemma_no_stop.append(word)

words_lemma_no_stop

"""###Data Frame zlemantyzowana i bez stop wordsów"""

all_no_stop_lemma = []

for text in all['Text']:
  doc = nlp(text)
  a = []
  for word in doc:
    a.append(word.lemma_)
      
  all_no_stop_lemma.append(' '.join(a))

all['Text'] = all_no_stop_lemma

all.sample(15)

"""##Podział datami"""

all_by_date = all.groupby('Date')['Text'].agg(lambda col: ''.join(col))

pd.DataFrame(all_by_date)

"""##Podział na sentencje"""

all_token_sentence = []
for text in all['Text']:
  all_token_sentence.append(sent_tokenize(text))

all_token_sentence

len(all_token_sentence)

"""#Eksploracja

###Mapa słów
"""

from PIL import Image
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator

df_words_lemma = pd.DataFrame(words_lemma_no_stop, columns=['Text'])

text = " ".join(x for x in words_lemma)
print ("Jest {} słów we wszystkich tekstach.".format(len(text)))

wordcloud = WordCloud(stopwords=pl_stop_words, max_font_size=50, max_words=100, background_color="white").generate(text)
plt.figure(figsize=(15, 7))
plt.imshow(wordcloud, interpolation="bilinear")
plt.axis("off")
plt.show()

"""### Występowanie spółek WIG20"""

from collections import Counter

wig_20 = {'Allegro' : ['allegro', 'ale'],  #
        'Asseco' : ['asseco', 'acp'], #
        'CD Project': ['cd projekt', 'cdr'],
        'CCC': ['ccc'], #
        'Cyfrowy Polsat': ['polsat', 'cps'], #
        'Dino Polska' : ['dino', 'dnp'],#
        'PGN SA' : ['polskie górnictwo naftowe i gazownictwo', 'pgn', 'pgnig'], 
        'Grupa Kety' : ['kty', 'kęty'], #
        'JSW' : ['jastrzębska spółka węglowa', 'jsw'],#
        'KGHM Polska Miedź' : ['kghm', 'kgh'], #
        'LPP' : ['lpp'], #
        'mBank' : ['mbank', 'mbk'] , #
        'Orange Polska' : ['orange', 'opl'], 
        'Pekao BP' : ['polska kasa opieki', 'pekao', 'peo'],#
        'Pepco' : ['pepco', 'pco'], #
        'PGE' : ['pge polska grupa energetyczna', 'pge'],#
        'Orlen' : ['polski koncern naftowy', 'orlen', 'pkn'],#
        'PKO BP' : ['powszechna kasa oszczednosci bank polski', 'pko'], #
        'PZU' : ['powszechny zakład ubezpieczeń', 'pzu'], #
        'Santander' : ['santander', 'spl']}#

wig_20_freq = []
for k, i in wig_20.items():
  for word in words_lemma_no_stop:
    if word in i:
      wig_20_freq.append(k)

wig_20_count = Counter(wig_20_freq)
wig_20_count = {k: v for k, v in sorted(wig_20_count.items(), key=lambda item: item[1], reverse=True)}

fig, ax = plt.subplots(figsize=(20,7))
ax.xaxis.set_tick_params(rotation=70)
ax.set_xlabel("Spółka")
ax.set_ylabel('Ilość')
plt.title("Występowanie spółek WIG20 w publikacjach")
plt.bar(range(len(wig_20_count)), list(wig_20_count.values()), align='center')
plt.xticks(range(len(wig_20_count)), list(wig_20_count.keys()))
plt.show()

"""##Sentyment

### Przypisanie sentymentu ze zbioru LOBI
"""

#LOBI do słownika
nawl_dict = pd.read_csv('/content/nawl-analysis.csv', header=None, index_col=0, squeeze=True).to_dict()
#wyciągnięcie tylko kategorii
nawl_cat = nawl_dict[1]
len(nawl_cat)

# Policzenie wystąpień słów z listy słów zlemmatyzowanych i przypisanie Kategorii
sent_category = []
for k, i in nawl_cat.items():
  for word in words_lemma_no_stop:
    if word in k:
      sent_category.append(i)

sent_category = Counter(sent_category)

#zmiana na df i zdropowanie kategorii, neutral, unclassified i zamiana na polskie nazwy
df_sent_category = pd.DataFrame.from_dict(sent_category, orient='index', dtype=None, columns=['Ilość'])
df_sent_category = df_sent_category.drop(['category', 'N', 'U'])
df_sent_category.rename(index={'H': 'Szczęście',
                               'A': 'Złość', 
                               'S': 'Smutek',
                               'F': 'Strach',
                               'D': 'Wstręt'}, inplace=True)
#sortowanie po ilości wystąpień
df_sent_category = df_sent_category.sort_values(by=['Ilość'],ascending=False)

df_sent_category

"""###Wykres sentymentu"""

height = df_sent_category.Ilość
bars = df_sent_category.index
x_pos = np.arange(len(df_sent_category))

fig, ax = plt.subplots(figsize=(14,7))
plt.bar(x_pos, height)


plt.xticks(x_pos, bars)

# Show graph
plt.show()

"""##Wykresy giełdowe

###Ceny akcji
"""

plt.figure(figsize=(20, 12))
for i in range(len(tiki_zamkniecie)):
    plt.subplot(5,4,i+1)
    plt.subplots_adjust( hspace=1 ,wspace=0.5)
    plt.title(tiki_zamkniecie[i])
    plt.plot(stocks[tiki_zamkniecie[i]])
    plt.xticks(rotation=90)
plt.suptitle("Ceny Akcji WIG 20")

"""###Zmiany procentowe"""

plt.figure(figsize=(20, 12))
for i in range(len(tiki_zmiana)):
    plt.subplot(5,4,i+1)
    plt.subplots_adjust( hspace=1 ,wspace=0.5)
    plt.title(tiki_zmiana[i])
    plt.plot(stocks[tiki_zmiana[i]])
    plt.xticks(rotation=90)
plt.suptitle("Zmiany procentowe spółek WIG20")

"""###Porównianie "dużego" WIG z WIG20"""

fig, ax = plt.subplots(2,figsize=(25,8))
ax[0].plot(wig['wig Zamkniecie'])
ax[1].plot(wig20['wig20 Zamkniecie'])

"""###Korelacja cen zamknięcia spółek WIG 20"""

correlation = stocks[tiki_zamkniecie].corr()
fig = plt.figure(figsize = (30, 15))
sns.heatmap(correlation, annot = True)
plt.show()

"""###Korelacja dziennych zmian spółek WIG 20"""

plt.figure(figsize=(30,15))
correlation = stocks[tiki_zmiana].corr()
sns.heatmap(correlation, annot=True)
plt.show()

"""###Korelacja dziennych wolumenów spółek WIG 20"""

plt.figure(figsize=(30,15))
correlation = stocks[tiki_wolumen].corr()
sns.heatmap(correlation, annot=True)
plt.show()

"""###Box plot z cenami"""

plt.figure(figsize=(30, 15))

for i in range(len(tiki_zamkniecie)):
    plt.subplot(5, 4, i+1)
    plt.subplots_adjust( hspace=0.5 )
    sns.boxplot(x=stocks[tiki_zamkniecie[i]])

"""###Box Plot z zmianami procentowymi"""

plt.figure(figsize=(30, 15))

for i in range(len(tiki_zmiana)):
    plt.subplot(5, 4, i+1)
    plt.subplots_adjust( hspace=0.5 )
    sns.boxplot(x=stocks[tiki_zmiana[i]])

"""#Topic modeling

LDA nienadzorowane uczenie maszynowe, które stosuje techniki grupowania do znajdowania ukrytych struktur.
"""

from gensim import corpora
from gensim.utils import tokenize


descriptions = [list(tokenize(t)) for t in all['Text']]

dictionary = corpora.Dictionary(descriptions)
corpus = [dictionary.doc2bow(text) for text in descriptions]

import gensim
NUM_TOPICS = 4
ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics = NUM_TOPICS, id2word=dictionary, passes=50)

topics = ldamodel.print_topics(num_words=4)
for topic in topics:
  print(topic)

!pip install pyLDAvis

import pyLDAvis.gensim_models
lda_display = pyLDAvis.gensim_models.prepare(ldamodel, corpus, dictionary, sort_topics=True)
pyLDAvis.save_html(lda_display, 'sk_topics.html')

"""#Klasteryzacja Hierarchiczna

##Dywersyfikacja spółek ze względu na dzienne zmiany cenowe
"""

price_change = get_price_change()

price_change_corr_matrix = price_change.corr()

from scipy.cluster.hierarchy import linkage, dendrogram


mergings = linkage(price_change_corr_matrix, method='complete')

fig, ax = plt.subplots(figsize=(15,5))
dendrogram(mergings,
           labels=price_change_corr_matrix.index,
           leaf_rotation=70,
           leaf_font_size=20,
            )
plt.show()

"""##Dywersyfikacja spółek ze względu na dzienne wolumeny ilościowe"""

wolumen = get_all_volumen()
wolumen_corr_matrix = wolumen.corr()

mergings = linkage(wolumen_corr_matrix, method='complete')

fig, ax = plt.subplots(figsize=(15,5))
dendrogram(mergings,
           labels=wolumen_corr_matrix.index,
           leaf_rotation=70,
           leaf_font_size=20,
            )
plt.show()